const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const app = express();
dotenv.config();
app.use(cors());
app.use(bodyParser.json());

const port = process.env.PORT || 3000;
const url = process.env.DATABASE_URL;
const UsersRouter = require('./routes/user.route');

var server = require('http').Server(app);
server.listen(port, () => {
  console.log('\x1b[36m%s\x1b[0m', 'VueSockets écoute désormais sur le port ', port);
});

var io = require("socket.io")(server, {
  cors: {
    origin: "*",
  }
});
app.use(function(req, _res, next) {
  req.io = io;
  next();
});

// Sockets functions
const User = require('./models/user');

io.on('connection', function (socket) {
  console.log('User ' + socket.id + ' connected');
  socket.emit('setID');
  socket.on('updateID', async (data) => {
    const user = await User.findOne({email: data.email});
    user.websocketID = socket.id;
    await user.save();
  });
});


(async () => {
  try {
      await mongoose.connect(url, {
          useNewUrlParser: true,
          useUnifiedTopology: true
      });
      console.log('\x1b[36m%s\x1b[0m', 'MongoDB Connected:', url);
  } catch (err) {
      console.error(err);
  }
})();

app.use("/users", UsersRouter);

app.get('/', (_req, res) => {
  res.send('Bienvenue sur l\'API de VueSockets 😉')
});