var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId = mongoose.Types.ObjectId;

var userSchema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  pseudo: {type: String, required: true},
  websocketID: {type: String, default: ''}
});

userSchema.set("toJSON", {
  transform: (document, returnObject) => {
    returnObject.id = returnObject._id.toString();
    delete returnObject._id;
    delete returnObject._v;
  },
});

module.exports = mongoose.model('User', userSchema);