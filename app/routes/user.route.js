const express = require("express");
const UsersRouter = express.Router();

const User = require('../models/user');

UsersRouter.post('/login', async (req, res) => {
  try {
    const user = await User.findOne({email: req.body.email});
    if (!user) {
      res.status(202).send("Cet email n'est relié à aucun compte");
      return;
    } else {
      if (req.body.password == user.password) {
        res.status(200).send(user);
        return;
      } else {
        res.status(202).send("Mot de passe incorrect");
        return;
      }
    }
  } catch (error) {
    console.log(error);
    res.status(400).send("Something went wrong..." + error);
  }
});

module.exports = UsersRouter;